#pragma once

#include <vector>

#include "Assert.hpp"
#include "Slice2D.hpp"
#include "Row.hpp"
#include "Shape2D.hpp"

template <typename T>
struct Array2D
{
public:
    using iterator               = typename Slice2D<T>::iterator;
    using const_iterator         = typename Slice2D<T>::const_iterator;
    using reverse_iterator       = typename Slice2D<T>::reverse_iterator;
    using const_reverse_iterator = typename Slice2D<T>::const_reverse_iterator;
    
    using value_type             = typename Slice2D<T>::value_type;
    using size_type              = typename Slice2D<T>::size_type;
    using difference_type        = typename Slice2D<T>::difference_type;
    using pointer                = typename Slice2D<T>::pointer;
    using reference              = typename Slice2D<T>::reference;
    using const_pointer          = typename Slice2D<T>::const_pointer;
    using const_reference        = typename Slice2D<T>::const_reference;
    
private:
    std::vector<std::remove_const_t<T>> data;
    Shape2D shape;

public:
    Array2D() = default;
    Array2D(Shape2D const& shape)         { Resize(shape); }
    Array2D(std::size_t w, std::size_t h) { Resize(w, h);  }

    Row<T>       operator[](std::size_t idx)       { return Slice()[idx]; }
    Row<T const> operator[](std::size_t idx) const { return Slice()[idx]; }

    Slice2D<T>       Slice(std::size_t x, std::size_t y, Shape2D const& sh)       { return Slice().Slice(x, y, sh); }
    Slice2D<T const> Slice(std::size_t x, std::size_t y, Shape2D const& sh) const { return Slice().Slice(x, y, sh); }
    
    Slice2D<T>       Slice(std::size_t x, std::size_t y, std::size_t w, std::size_t h)       { return Slice(x, y, Shape2D(w, h)); }
    Slice2D<T const> Slice(std::size_t x, std::size_t y, std::size_t w, std::size_t h) const { return Slice(x, y, Shape2D(w, h)); }
    
    Slice2D<T>       Slice()       { return Slice2D<T>(&data[0], shape, shape.Width());       }
    Slice2D<T const> Slice() const { return Slice2D<T const>(&data[0], shape, shape.Width()); }
    
    operator Array2D<std::remove_const_t<T>> const& () const
    {
        return *(Array2D<std::remove_const_t<T>>*)(this);
    }
    
    operator Array2D<T const> const& () const
    {
        return *(Array2D<T const> const*)(this);
    }

    operator Slice2D<T>()             { return Slice(); }
    operator Slice2D<T const>() const { return Slice(); }

    Shape2D     Shape()  const { return shape;          }
    std::size_t Width()  const { return shape.Width();  }
    std::size_t Height() const { return shape.Height(); }
    
    T      * Data()       { return data.data(); }
    T const* Data() const { return data.data(); }

    void Resize(Shape2D const& shape)
    {
        std::size_t count = shape.Width() * shape.Height();
        data.reserve(count);
        data.resize(count);
        this->shape = shape;
    }
    
    void Resize(std::size_t w, std::size_t h)
    {
        Resize(Shape2D(w, h));
    }

    Slice2D<T> operator*() { return Slice(); }
    
    constexpr iterator               begin()         noexcept { return Slice().begin();   }
    constexpr const_iterator         begin()   const noexcept { return Slice().begin();   }
    constexpr const_iterator         cbegin()  const noexcept { return Slice().cbegin();  }
    constexpr iterator               end()           noexcept { return Slice().end();     }
    constexpr const_iterator         end()     const noexcept { return Slice().end();     }
    constexpr const_iterator         cend()    const noexcept { return Slice().cend();    }
    constexpr reverse_iterator       rbegin()        noexcept { return Slice().rbegin();  }
    constexpr const_reverse_iterator rbegin()  const noexcept { return Slice().rbegin();  }
    constexpr const_reverse_iterator crbegin() const noexcept { return Slice().crbegin(); }
    constexpr reverse_iterator       rend()          noexcept { return Slice().rend();    }
    constexpr const_reverse_iterator rend()    const noexcept { return Slice().rend();    }
    constexpr const_reverse_iterator crend()   const noexcept { return Slice().crend();   }
};
