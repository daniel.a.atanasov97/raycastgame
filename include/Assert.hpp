#pragma once

#define STRINGIFY_(x) #x
#define STRINGIFY(x) STRINGIFY_(x)

#ifdef NDEBUG
#define ASSERT(x)
#else
#define ASSERT(x) if (!(x)) { throw std::runtime_error(__FILE__ ":" STRINGIFY(__LINE__) ": Assertion failed: " #x); }
#endif
