#pragma once

#include <type_traits>

#include "Shape2D.hpp"

template <typename T>
struct Array2D;

template <typename T>
struct Slice2D;

template <typename T>
struct Row;

namespace Helpers
{
    template <typename I1, typename I2, typename F>
    constexpr I2 Zip(I1 begin, I1 end, I2 other, F op)
    {
        while (begin != end) op(*begin++, *other++);
        return other;
    }
    
    template <typename I1, typename I2, typename F>
    constexpr I2 ZipSwapped(I1 begin, I1 end, I2 other, F op)
    {
        while (begin != end) op(*other++, *begin++);
        return other;
    }

    template <typename I, typename T, typename F>
    constexpr void ZipValue(I begin, I end, T && other, F op)
    {
        while (begin != end) op(*begin++, std::forward<T>(other));
    }
    
    template <typename I, typename T, typename F>
    constexpr void ZipValueSwapped(I begin, I end, T && other, F op)
    {
        while (begin != end) op(std::forward<T>(other), *begin++);
    }

    template <typename T>
    struct ArrayOpsImpl
    {
        using type = T;
        
        constexpr static bool is_array = false;
        constexpr static auto & Get(T & t, std::size_t y, std::size_t x) { return t; }
        constexpr static std::size_t Width(T const& t)  { return 1; }
        constexpr static std::size_t Height(T const& t) { return 1; }
    };

    template <typename T>
    struct ArrayOpsImpl<Array2D<T>>
    {
        using type = typename Array2D<T>::value_type;
        
        constexpr static bool is_array = true;
        constexpr static auto & Get(Array2D<T> & t, std::size_t y, std::size_t x) { return t[y % t.Height()][x % t.Width()]; }
        constexpr static std::size_t Width(Array2D<T> const& t)  { return t.Width();  }
        constexpr static std::size_t Height(Array2D<T> const& t) { return t.Height(); }
    };
    
    template <typename T>
    struct ArrayOpsImpl<Slice2D<T>>
    {
        using type = typename Slice2D<T>::value_type;
        
        constexpr static bool is_array = true;
        constexpr static auto & Get(Slice2D<T> & t, std::size_t y, std::size_t x) { return t[y % t.Height()][x % t.Width()]; }
        constexpr static std::size_t Width(Slice2D<T> const& t)  { return t.Width();  }
        constexpr static std::size_t Height(Slice2D<T> const& t) { return t.Height(); }
    };
    
    template <typename T>
    struct ArrayOpsImpl<Row<T>>
    {
        using type = typename Row<T>::value_type;
        
        constexpr static bool is_array = true;
        constexpr static auto & Get(Row<T> & t, std::size_t y, std::size_t x) { return t[x % t.Size()]; }
        constexpr static std::size_t Width(Row<T> const& t)  { return t.Size(); }
        constexpr static std::size_t Height(Row<T> const& t) { return 1;        }
    };

    template <typename T>
    using ArrayOps = ArrayOpsImpl<std::decay_t<T>>;

    template <typename... Ts>
    constexpr auto MakeArray(Ts... args)
    {
        return std::array<std::size_t, sizeof...(Ts)>{args...};
    }
    
    template <typename... Ts>
    constexpr std::size_t ArgMaxWidth(Ts const&... args)
    {
        auto a = MakeArray(ArrayOps<Ts>::Width(args)...);
        return std::distance(std::begin(a), std::max_element(std::begin(a), std::end(a)));
    }

    template <typename... Ts>
    constexpr std::size_t ArgMaxHeight(Ts const&... args)
    {
        auto a = MakeArray(ArrayOps<Ts>::Height(args)...);
        return std::distance(std::begin(a), std::max_element(std::begin(a), std::end(a)));
    }
    
    template <typename... Ts>
    constexpr std::size_t MaxWidth(Ts const&... args)
    {
        auto a = MakeArray(ArrayOps<Ts>::Width(args)...);
        return *std::max_element(std::begin(a), std::end(a));
    }

    template <typename... Ts>
    constexpr std::size_t MaxHeight(Ts const&... args)
    {
        auto a = MakeArray(ArrayOps<Ts>::Height(args)...);
        return *std::max_element(std::begin(a), std::end(a));
    }
    
    template <typename T, typename... Ts>
    constexpr bool CanBroadcast(Shape2D const& shape, T const& t, Ts const&... args)
    {
        if constexpr (sizeof...(Ts) == 0)
        {
            return
                (shape.Width() % ArrayOps<T>::Width(t) == 0) &&
                (shape.Height() % ArrayOps<T>::Height(t) == 0);
        }
        else
        {
            return CanBroadcast(shape, t) && CanBroadcast(shape, args...);
        }
    }
    
    template <typename... Ts>
    constexpr Shape2D BroadcastShape(Ts... args)
    {
        ASSERT(ArgMaxWidth(args...) == ArgMaxHeight(args...));
        return Shape2D(MaxWidth(args...), MaxHeight(args...));
    }
    
    template <typename T, typename... Ts>
    constexpr void VisitWithShape(Shape2D const& shape, T && function, Ts &&... args)
    {
        ASSERT(CanBroadcast(shape, args...));
        for (std::size_t y = 0; y < shape.Height(); y++)
        {
            for (std::size_t x = 0; x < shape.Width(); x++)
            {
                std::forward<T>(function)(ArrayOps<Ts>::Get(args, y, x)...);
            }
        }
    }
    
    template <typename T, typename... Ts>
    constexpr void Visit(T && function, Ts &&... args)
    {
        VisitWithShape(BroadcastShape(args...), std::forward<T>(function), args...);
    }

    template <typename T, typename... Ts>
    constexpr auto Gather(T && function, Ts &&... args)
    {
        Shape2D shape = BroadcastShape(args...);

        std::cout << shape.Width() << ' ' << shape.Height() << std::endl;
        
        ASSERT(CanBroadcast(shape, args...));
        
        using type = decltype(std::forward<T>(function)(std::declval<typename ArrayOps<Ts>::type>()...));
        
        Array2D<type> array(shape);
        auto wrapper = [&](auto & lhs, auto &... rhs)
        {
            lhs = (std::forward<T>(function)(rhs...));
        };
        VisitWithShape(shape, wrapper, array, args...);
        return array;
    }
}
