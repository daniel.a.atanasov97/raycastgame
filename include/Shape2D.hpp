#pragma once

struct Shape2D
{
private:
    std::size_t w = 0;
    std::size_t h = 0;

public:
    constexpr Shape2D() = default;
    constexpr Shape2D(std::size_t w, std::size_t h) : w(w), h(h) { }
    
    constexpr std::size_t Width()  const { return w; }
    constexpr std::size_t Height() const { return h; }
};
