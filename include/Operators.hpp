#pragma once

#include "Helpers.hpp"

namespace Ops
{
#define BINARY_OP(name, op)                                  \
    struct name {                                            \
        template <typename U, typename V>                    \
        constexpr auto operator()(U && u, V && v)            \
        { return std::forward<U>(u) op std::forward<V>(v); } \
    }

    BINARY_OP(Assign,       =);
    BINARY_OP(AssignAdd,    +=);
    BINARY_OP(AssignSub,    -=);
    BINARY_OP(AssignMul,    *=);
    BINARY_OP(AssignDiv,    /=);
    BINARY_OP(AssignMod,    %=);
    BINARY_OP(AssignBAnd,   &=);
    BINARY_OP(AssignBOr,    |=);
    BINARY_OP(AssignXor,    ^=);
    BINARY_OP(AssignLShift, <<=);
    BINARY_OP(AssignRShift, >>=);

    BINARY_OP(Add,    +);
    BINARY_OP(Sub,    -);
    BINARY_OP(Mul,    *);
    BINARY_OP(Div,    /);
    BINARY_OP(Mod,    %);
    BINARY_OP(BAnd,   &);
    BINARY_OP(BOr,    |);
    BINARY_OP(Xor,    ^);
    BINARY_OP(LShift, <<);
    BINARY_OP(RShift, >>);
    
    BINARY_OP(LAnd, &&);
    BINARY_OP(LOr,  ||);
    
    BINARY_OP(Equals,     ==);
    BINARY_OP(NotEquals,  !=);
    BINARY_OP(Less,       <);
    BINARY_OP(LessEquals, <=);
    BINARY_OP(More,       >);
    BINARY_OP(MoreEquals, >=);
    // BINARY_OP(Spaceship,  <=>);
    
#undef BINARY_OP    
}

#define VISIT_FUNCTIONS_IMPL(type, ...)                                 \
    template <typename F, typename U>                                   \
    constexpr void Visit(F && f, type<U> other) __VA_ARGS__             \
    { Helpers::Visit(std::forward<F>(f), *this, other); }               \
    template <typename F, typename U>                                   \
    constexpr void Visit(F && f, U && u) __VA_ARGS__                    \
    { Helpers::Visit(std::forward<F>(f), *this, std::forward<U>(u)); }

#define VISIT_FUNCTIONS(type)          \
    VISIT_FUNCTIONS_IMPL(type)         \
    VISIT_FUNCTIONS_IMPL(type, const)

#define BINARY_ASSIGN(type, op, fn, ...)                   \
    template <typename U>                                  \
    constexpr type<T> & operator op (U && other) __VA_ARGS__ \
    { Helpers::Visit(fn, *this, other); return *this; }

#define BINARY_RETURN(type, op, fn, ...)                        \
    template <typename U>                                       \
    constexpr auto operator op (type<U> other) __VA_ARGS__ {    \
                                                                \
        


#define BINARY_ASSIGN_OPERATORS(type)             \
    BINARY_ASSIGN(type, =,   Ops::Assign(), &&)   \
    BINARY_ASSIGN(type, +=,  Ops::AssignAdd())    \
    BINARY_ASSIGN(type, -=,  Ops::AssignSub())    \
    BINARY_ASSIGN(type, *=,  Ops::AssignMul())    \
    BINARY_ASSIGN(type, /=,  Ops::AssignDiv())    \
    BINARY_ASSIGN(type, %=,  Ops::AssignMod())    \
    BINARY_ASSIGN(type, &=,  Ops::AssignBAnd())   \
    BINARY_ASSIGN(type, |=,  Ops::AssignBOr())    \
    BINARY_ASSIGN(type, ^=,  Ops::AssignXor())    \
    BINARY_ASSIGN(type, <<=, Ops::AssignLShift()) \
    BINARY_ASSIGN(type, >>=, Ops::AssignRShift())
