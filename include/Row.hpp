#pragma once

#include <iterator>

#include "Assert.hpp"
#include "Operators.hpp"
#include "Helpers.hpp"
#include "Shape2D.hpp"

template <typename T>
struct Array2D;

template <typename T>
struct Row
{
public:
    using difference_type        = std::size_t;
    using value_type             = T;
    using pointer                = T*;
    using const_pointer          = T const*;
    using reference              = T&;
    using const_reference        = T const&;
    using iterator               = value_type*;
    using const_iterator         = value_type const*;
    using reverse_iterator       = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    using iterator_category      = std::random_access_iterator_tag;
    
private:
    T * ptr;
    std::size_t size;

public:
    constexpr Row(Row<T> const& other) : ptr(other.ptr), size(other.size) { }
    constexpr Row(T * ptr, std::size_t size) : ptr(ptr), size(size)
    {
        ASSERT(ptr != nullptr);
        ASSERT(size >= 0);
    }

    constexpr T & operator[](std::size_t idx)
    {
        ASSERT(idx >= 0 && idx < size);
        return ptr[idx];
    }

    constexpr T const& operator[](std::size_t idx) const
    {
        ASSERT(idx >= 0 && idx < size);
        return ptr[idx];
    }
    
    constexpr operator Row<std::remove_const_t<T>> const& () const
    {
        return *(Row<std::remove_const_t<T>>*)(this);
    }
    
    constexpr operator Row<T const> const& () const
    {
        return *(Row<T const> const*)(this);
    }

    constexpr std::size_t Size()  const { return size;             }
    constexpr Shape2D     Shape() const { return Shape2D(size, 1); }
    
    constexpr T      * Data()       { return ptr; }
    constexpr T const* Data() const { return ptr; }

    constexpr Array2D<std::remove_const_t<T>> Copy() const
    {
        Array2D<std::remove_const_t<T>> copy(Size(), 1);
        *copy = *this;
        return copy;
    }

    constexpr Row<T> operator*() { return *this; }

    constexpr Row<T> & operator=(Row<T> const& other) &
    {
        ptr = other.ptr;
        size = other.size;
    }
    
    VISIT_FUNCTIONS(Row);
    BINARY_ASSIGN_OPERATORS(Row);
    
    constexpr iterator               begin()         noexcept { return iterator(ptr);                   }
    constexpr const_iterator         begin()   const noexcept { return const_iterator(ptr);             }
    constexpr const_iterator         cbegin()  const noexcept { return const_iterator(ptr);             }
    constexpr iterator               end()           noexcept { return iterator(ptr + size);            }
    constexpr const_iterator         end()     const noexcept { return const_iterator(ptr + size);      }
    constexpr const_iterator         cend()    const noexcept { return const_iterator(ptr + size);      }
    constexpr reverse_iterator       rbegin()        noexcept { return reverse_iterator(end());         }
    constexpr const_reverse_iterator rbegin()  const noexcept { return reverse_iterator(end());         }
    constexpr const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator(end());   }
    constexpr reverse_iterator       rend()          noexcept { return reverse_iterator(begin());       }
    constexpr const_reverse_iterator rend()    const noexcept { return reverse_iterator(begin());       }
    constexpr const_reverse_iterator crend()   const noexcept { return const_reverse_iterator(begin()); }
};
