#include <SDL2/SDL.h>
#include <SDL2/SDL_render.h>

#include <array>
#include <string>
#include <cstdint>

#include <iostream>

#include <Array2D.hpp>

template <typename T, std::size_t S>
struct StaticArray
{
private:
    T data[S];

public:
    constexpr StaticArray() = default;

    constexpr StaticArray(std::initializer_list<T> list)
    {
        std::copy(std::begin(list), std::next(std::begin(list), std::min(list.size(), S)), std::begin(data));
    }

    constexpr std::size_t Size() const { return S; }

    constexpr T      & operator[](int idx)       { return data[idx]; }
    constexpr T const& operator[](int idx) const { return data[idx]; }
};

struct Color
{
    uint8_t r{};
    uint8_t g{};
    uint8_t b{};
    uint8_t a{};

    constexpr Color() = default;
    constexpr Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255) : r(r), g(g), b(b), a(a) { }
    
    constexpr uint8_t & operator[](int idx)
    {
        ASSERT(idx >= 0 && idx < 4);
        switch (idx)
        {
        case 0: return r;
        case 1: return g;
        case 2: return b;
        case 3: return a;
        }
    }

    constexpr uint8_t const& operator[](int idx) const
    {
        ASSERT(idx >= 0 && idx < 4);
        switch (idx)
        {
        case 0: return r;
        case 1: return g;
        case 2: return b;
        case 3: return a;
        }
    }
};

constexpr double Lerp(double a, double b, double t)
{
    return (1 - t) * a + t * b;
}

constexpr Color Mix(Color a, Color b, double t = 0.5)
{
    return Color(
        (uint8_t)round(sqrt(Lerp(a.r * a.r, b.r * b.r, t))),
        (uint8_t)round(sqrt(Lerp(a.g * a.g, b.g * b.g, t))),
        (uint8_t)round(sqrt(Lerp(a.b * a.b, b.b * b.b, t))),
        (uint8_t)round(sqrt(Lerp(a.a * a.a, b.a * b.a, t)))
    );
}

void DrawBorder(Slice2D<Color> image)
{
    image[0]                                                 = Color(255, 255, 255);
    image.Slice(0, 1, 1, image.Height() - 2)                 = Color(255, 255, 255);
    image[image.Height() - 1]                                = Color(255, 255, 255);
    image.Slice(image.Width() - 1, 1, 1, image.Height() - 2) = Color(255, 255, 255);
}

struct Window
{
private:
    Array2D<Color> screen;

    SDL_Window   * window;
    SDL_Renderer * renderer;
    SDL_Texture  * texture;
    
public:
    Window(std::string const& name, int x, int y, int w, int h, int sw, int sh)
    {
        SDL_Init(SDL_INIT_EVERYTHING);
        window = SDL_CreateWindow(name.c_str(), x, y, w, h, SDL_WINDOW_SHOWN);
        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
        texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, sw, sh);
        screen.Resize(sw, sh);
    }

    void DrawMinimap(Slice2D<Color> image)
    {
        DrawBorder(image);
    }
    
    void Loop()
    {
        bool running = true;
        while (running)
        {
            for (SDL_Event event; SDL_PollEvent(&event);)
            {
                switch (event.type)
                {
                case SDL_QUIT:
                    running = false;
                    break;
                case SDL_KEYDOWN:
                    running = event.key.keysym.scancode != SDL_SCANCODE_ESCAPE;
                    break;
                }
            }
            
            Render();
            
            SDL_Delay(100);
        }
    }

    void Render()
    {
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderClear(renderer);

        int sx = 0;
        int sy = 0;
        int sw = 200;
        int sh = 200;

        Slice2D<Color> pixels = screen.Slice(sx, sy, sw, sh);
            
        DrawMinimap(pixels);
        
        SDL_UpdateTexture(texture, NULL, screen.Data(), screen.Width() * sizeof(Color));

        SDL_RenderCopy(renderer, texture, NULL, NULL);
        SDL_RenderPresent(renderer);
    }
    
    Color const* Pixels() const
    {
        return screen.Data();
    }

    ~Window()
    {
        SDL_DestroyTexture(texture);
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
    }
};

int main(int argc, char ** argv)
{
    int x = SDL_WINDOWPOS_UNDEFINED;
    int y = SDL_WINDOWPOS_UNDEFINED;
    int w = 600;
    int h = 600;
    int sw = 1000;
    int sh = 1000;
    
    Window window("Game", x, y, w, h, sw, sh);
    window.Loop();
    
    return 0;
}
